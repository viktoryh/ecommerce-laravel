@extends('layouts.app_admin')

@section('content')
<div class="card card-cascade narrower">
    <div class="table-responsive"> 
        <table class="table">
            <thead class="blue-grey lighten-4">
                <tr>
                    <th>#</th>
                    <th>{{__('admin_pages.client_name')}}</th>
                    <th>{{__('admin_pages.client_email')}}</th>
                    <th>{{__('admin_pages.client_phone')}}</th>
                    <th>{{__('admin_pages.client_address')}}</th>
                    <th class="text-right">
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clients as $client) 
                <tr>
                    <th scope="row">{{$client->id}}</th>
                    <td>{{$client->first_name}}</td>
                    <td>{{$client->email}}</td>
                    <td>{{$client->phone}}</td>
                    <td>{{$client->address}}</td>
                    <td class="text-right">
                    </td>
                </tr> 
                @endforeach
            </tbody>
        </table>
    </div>  
</div>

@endsection
